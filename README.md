# **Blocn't**
![forthebadge](https://forthebadge.com/images/badges/built-for-android.svg)
![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)
<br></br>
Classic notepad done on my spare time, again yes.
<br></br>
<br></br>
# The Game
Classic notepad with no much things to say. Some pictures of the app to show you a first look :

<br></br>
<img src="https://gitlab.com/JumpyfrOg/blocn_t/-/raw/master/img_readme/menu_example.png"  width="300" height="650">
The menu of the app which show the list of saved notes.

<br></br>
<img src="https://gitlab.com/JumpyfrOg/blocn_t/-/raw/master/img_readme/modif_example.png"  width="300" height="650">
The edit page of the app.
<br></br>

# Installation and start
To run the application, you need to download the file "[*blocn_t.apk*](https://gitlab.com/JumpyfrOg/blocn_t/-/raw/master/blocn_t.apk)" at the root of the project. Then executed it on a smartphone with the operating system android and its done.
<br></br>
# Service used
- Online photo editor : [Pixlr](https://pixlr.com/)
- IDE : [Android Studio](https://developer.android.com/studio)
- Online markdown editor : [Stackedit](https://stackedit.io/)
<br></br>
# Contact
- Mail : mcharron29@gmail.com
- Twitter : [Charron M](https://twitter.com/CharronM5)
# License

[GPL] License

 [GPL]: LICENSE
