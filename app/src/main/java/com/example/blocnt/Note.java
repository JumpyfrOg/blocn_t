package com.example.blocnt;

import java.io.Serializable;

/**
 * Class which represent a note. It contains a title and a content.
 * @author Charron M.
 * @version 1.0
 * License : GPL or later
 */
public class Note implements Serializable {

    // the title of the note
    private String title;

    // the content of the note
    private String content;

    /**
     * Constructor of the class that requires a title. A content can be passed by parameter.
     * @param title The title of the note
     * @param content The content of the note
     */
    public Note (String title, String content) {
        if ( title != null ) {
            this.title = title;
            this.content = content;
        }
    }


    // Getter / Setter
    public void setTitle(String title){this.title=title;}
    public String getTitle(){return this.title;}
    public void setContent(String content){this.content=content;}
    public String getContent(){return this.content;}
}
