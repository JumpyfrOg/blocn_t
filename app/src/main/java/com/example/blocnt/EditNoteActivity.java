package com.example.blocnt;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Activity in charge to manage the edition of a note with the edition note layout.
 * @author Charron M.
 * @version 1.0
 * License : GPL or later
 */
public class EditNoteActivity extends AppCompatActivity {

    // the editing note
    private ArrayList<Note> list_note;

    // the position of the editing note in the list, if a note is modifying
    private int position;

    // boolean which say if the activity is called to add a note, else to modify one.
    private boolean addANote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_note_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // get data pass through changing activity
        Intent intent = getIntent();

        // if the activity has received like value, the role of the page with the key "action"
        if ( intent.getExtras().containsKey("action") ) {

            //initialize the variable which store the list of task
            this.list_note = (ArrayList<Note>) intent.getBundleExtra("BUNDLE").getSerializable("ARRAYLIST");
            if ( intent.getExtras().containsKey("place") ) {
                this.position = intent.getExtras().getInt("place");
            }else{
                this.position = -1;
            }

            // if the user wants to add a new task
            if ( intent.getExtras().getString("action").equals("add") ) {
                this.initAddActivity();
            }else if ( intent.getExtras().getString("action").equals("edit") ) {
                this.initModifActivity();
            }
            // TODO : synchronize key action and position value
        }
    }


    /**
     * Initialize the activity and the layout to show a page for adding a note (label of the buttons
     * , listener of the buttons ...)
     */
    public void initAddActivity () {

        // create a new note
        this.addANote = true;

        // config the remove_back button to come back to the previous activity
        Button remove_back = (Button) findViewById(R.id.remove_back_button);
        remove_back.setText(getResources().getString(R.string.button_back));
        remove_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // create a new intent to prepare switching activity
                Intent intent = new Intent(EditNoteActivity.this,MainActivity.class);
                // put a flag to specify this activity is closed and go back to the previous which
                // is the main activity of the app
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // start activity
                startActivity(intent);
            }
        });

        // config the add_save button to add the note in the list saved in the application
        Button add_save = (Button) findViewById(R.id.add_save_button);
        add_save.setText(getResources().getString(R.string.button_add));
        add_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText title_input = findViewById(R.id.input_title);
                EditText content_input = findViewById(R.id.input_content);
                if ( title_input.getText().toString() != null && !title_input.getText().toString().equals("") ) {

                    list_note.add(new Note(title_input.getText().toString(), content_input.getText().toString()));

                    // save the note in the file
                    save();

                    // create a new intent to prepare switching activity
                    Intent intent = new Intent(EditNoteActivity.this, MainActivity.class);

                    // put a flag to specify this activity is closed and go back to the previous which
                    // is the main activity of the app
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // TODO : generalize back switching activity

                    // start activity
                    startActivity(intent);
                }else{
                    Toast.makeText(EditNoteActivity.this,"Rentrez au moins un titre",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    /**
     * Initialize the activity and the layout to show a page for modifying a note (label of the
     * buttons, listener of the buttons ...)
     */
    public void initModifActivity () {

        // config the remove_back button to remove the current editing note
        Button remove_back = (Button) findViewById(R.id.remove_back_button);
        remove_back.setText(getResources().getString(R.string.button_delete));

        // config the add_save button to save the note
        Button add_save = (Button) findViewById(R.id.add_save_button);
        add_save.setText(getResources().getString(R.string.button_save));

        // put the title and the content in the components
        EditText title_input = findViewById(R.id.input_title);
        title_input.setText(this.list_note.get(position).getTitle());
        EditText content_input = findViewById(R.id.input_content);
        if ( this.list_note.get(position).getContent() != null ) {
            content_input.setText(this.list_note.get(position).getContent());
        }

        // display a popup to confirm the deletion choice
        remove_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delete_popup();
            }
        });

        add_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText title_input = findViewById(R.id.input_title);
                EditText content_input = findViewById(R.id.input_content);
                if ( title_input.getText().toString() != null && !title_input.getText().toString().equals("") ) {
                    list_note.get(position).setTitle(title_input.getText().toString());
                    list_note.get(position).setContent(content_input.getText().toString());

                    // save modification in the file
                    save();

                    // create a new intent to prepare switching activity
                    Intent intent = new Intent(EditNoteActivity.this, MainActivity.class);

                    // put a flag to specify this activity is closed and go back to the previous which
                    // is the main activity of the app
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    // start activity
                    startActivity(intent);
                }else{
                    Toast.makeText(EditNoteActivity.this,"Rentrez au moins un titre",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    /**
     * Method will show a popup. If yes is clicked the note is deleted and the main activity
     * is launched. If no, the popup closes.
     */
    public void delete_popup () {
        new AlertDialog.Builder(this)
                .setTitle("Attention")
                .setMessage("Voulez vous vraiment supprimer cette note ?")
                // when "NON" is clicked
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // close popup
                    }
                })
                // when "OUI" button is clicked
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        list_note.remove(position);
                        // save modification in the file
                        save();

                        // create a new intent to prepare switching activity
                        Intent intent = new Intent(EditNoteActivity.this,MainActivity.class);

                        // put a flag to specify this activity is closed and go back to the previous
                        // which is the main activity of the app
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        // start activity
                        startActivity(intent);
                    }
                })
                .show();
    }


    /**
     * Method to save the list of notes in a file
     */
    public void save () {
        try {

            // get the file to store notes, or create it
            File directory = getFilesDir();
            File file = new File(directory, "data_blocnt.txt");

            // create stream to pass data between the file and the app
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            // for each notes in the list, save one by one in the file
            for ( Note task : this.list_note ) {
                oos.writeObject(task);
            }

            // close stream
            oos.close();
            fos.close();

            // inform the user that the save is successful
            Toast.makeText(this,"Modification effectuées",Toast.LENGTH_LONG).show();

        // if an exception is throw during the saving, inform the user that the backup didn't work
        }catch(Exception e){
            Toast.makeText(this,"Echec de la sauvegarde définitive",Toast.LENGTH_LONG).show();
        }
    }

}
