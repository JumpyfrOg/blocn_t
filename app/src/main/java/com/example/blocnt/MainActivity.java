package com.example.blocnt;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 * The activity of the menu of the app. There is a list of saved notes.
 * @author Charron M.
 * @version 1.0
 * License : GPL or later
 */
public class MainActivity extends AppCompatActivity {

    // The list of notes saved in the application
    private ArrayList<Note> list_note;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.list_note = new ArrayList<>();
        // call the method to extract the data saved on the phone
        this.load();

        // if the list of task isn't empty
        if ( this.list_note.size() != 0 ) {
            // then hide the label which show "You don't have any task" displayed where the list of
            // task is empty, so when the user has 0 task. By default the label is shown in
            // activity_main.xml
            TextView mess = (TextView) findViewById(R.id.message_void);
            mess.setVisibility(View.GONE);
        }


        // create a adapter to display the note of the list, on the screen
        AdapterList arrayAdap = new AdapterList(this,this.list_note);

        // put the layout of the list of note
        ListView view = findViewById(R.id.list_layout);
        view.setAdapter(arrayAdap);

        // set a listener on each notes in the list
        this.initListListerner();

        // set fab listener
        this.setFloatingButtonListener();
    }


    /**
     * Set a listener to the floating button. Button in charge of displaying activity to add a note.
     */
    public void setFloatingButtonListener () {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // create a new intent to prepare switching activity
                Intent intent = new Intent(MainActivity.this,EditNoteActivity.class);

                // put in the switching intent, the type of the action done. "edit" if the next
                // activity must modify the selected note, "add" if the next activity must add a new
                // note
                intent.putExtra("action","add");
                // put the list of note
                Bundle list = new Bundle();
                list.putSerializable("ARRAYLIST",list_note);
                intent.putExtra("BUNDLE",list);

                // put a flag to specify the next activity like a new activity on this activity (the
                // menu of the app)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                // start activity
                startActivity(intent);
            }
        });
    }


    /**
     * Method to initialize the reaction of the app to a click on a note displayed in the list
     */
    public void initListListerner () {
        // get the view of list of item of the application
        ListView view = findViewById(R.id.list_layout);

        // add a action to a click from the user on a note
        view.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {

                // create a new intent to prepare switching activity
                Intent intent = new Intent(MainActivity.this,EditNoteActivity.class);

                // put in the switching intent, the type of the action done. "edit" if the next
                // activity must modify the selected note, "add" if the next activity must add a new
                // note
                intent.putExtra("action","edit");
                // put the list of note, and the position of the selected note in this list
                intent.putExtra("place",position);
                // put the list of note
                Bundle list = new Bundle();
                list.putSerializable("ARRAYLIST",list_note);
                intent.putExtra("BUNDLE",list);

                // put a flag to specify the next activity like a new activity on this activity (the
                // menu of the app)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                // start the new activity
                startActivity(intent);

            }
        });
    }


    /**
     * Method to load the notes of the application saved on a app file. The notes are stock in
     * the attribute "list_note"
     */
    public void load () {
        try {

            // Get the file where data are saved
            File directory = getFilesDir();
            File file = new File(directory, "data_blocnt.txt");


            boolean cont = true;

            // Create stream to pass data from the file through the application
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream is = new ObjectInputStream(fis);

            // if the reader isn't at the end of the file
            while (cont) {
                // get the next object wrote in the file
                Object obj = is.readObject();
                if (obj != null) {
                    this.list_note.add((Note) obj);
                } else {
                    cont = false;
                }
            }

            // close  stream
            is.close();
            fis.close();


        // If the file doesn't exist (exception made just at the begining, at the first launch
        // of the app)
        } catch (FileNotFoundException e) {
            // Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG).show();

        // If a exception is throw during the lecture of the file
        } catch (IOException e) {
//            if ( e.getClass() != EOFException.class ) {
//                Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG).show();
//            }

        // If any app's classes correspond to the extracted data
        }catch (ClassNotFoundException e) {
            Toast.makeText(MainActivity.this, "L'application a rencontré des erreurs lors du chargement des données", Toast.LENGTH_LONG).show();
        }
    }








    //Default methods

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
