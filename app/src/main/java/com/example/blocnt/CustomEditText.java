package com.example.blocnt;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * A custom EditText that draws lines between each displayed line of text.
 * @author Charron M.
 * @version 1.0
 * License : GPL or later
 */
public class CustomEditText extends EditText {
    private Rect mRect;
    private Paint mPaint;

    /**
     * Constructor of the class to initialize the configuration of the designated lines
     */
    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        mRect = new Rect();
        mPaint = new Paint();
        // init the style of drawing and the color
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(R.color.colorPrimary);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int count = getLineCount();
        Rect r = mRect;
        Paint paint = mPaint;

        for (int i = 0; i < count; i++) {
            int baseline = getLineBounds(i, r);

            canvas.drawLine(r.left, baseline + 10, r.right, baseline + 10, paint);
        }

        super.onDraw(canvas);
    }
}
