package com.example.blocnt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Class which display in the list the different notes saved in the application.
 * @author Charron M.
 * @version 1.0
 * License : GPL or later
 */
public class AdapterList extends BaseAdapter {

    // the list of notes to display
    private ArrayList<Note> list_note;

    // The context of the application where the display should take place
    private Context context;

    private LayoutInflater inflater;


    /**
     * Constructor of the class
     * @param context The context where display the list
     * @param list The list of task to display
     */
    public AdapterList(Context context, ArrayList<Note> list) {
        this.context = context;
        this.list_note = list;
        inflater = LayoutInflater.from(context);
    }


    /**
     * Get the number of item displayed in the list.
     * @return The number of item displayed.
     */
    public int getCount () {
        return this.list_note.size();
    }


    /**
     * Method which return the context where the adapter adapt the display of the list;
     * @return The context where is the list.
     */
    public Context getContext () {
        return this.context;
    }


    /**
     * Get the item according the position passed in parameter.
     * @param position The position of the item to get.
     * @return The item at this position in the list.
     */
    public Object getItem(int position){
        return list_note.get(position);
    }


    public long getItemId(int position){
        return position;
    }


    /**
     * Method to make item and add it to the displayed list of notes. Method automatically called by
     * the program.
     * @param position The position of the item in the list to create.
     * @param convertView The view used to make a item.
     * @param parent The parent that this view will eventually be attached to.
     * @return The item generated
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        //if (convertView == null) {
            view = (View) inflater.inflate(R.layout.item_note, parent, false);
        //}else{
          //  view = (View) convertView;
        //}

        // get the label and set the title of the note in it to display it
        TextView title = view.findViewById(R.id.title_note);
        title.setText(this.context.getResources().getString(R.string.title_sign) + " " + this.list_note.get(position).getTitle());

        return view;
    }
}

